﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ShiQuan.Access.Helper;

namespace ShiQuan.WinControls
{
    public class UCCommand: UserControl
    {
        private ToolStrip sysToolBar;
        private ToolStripButton tbbExcute;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private RichTextBox txtResult;
        private Splitter splitter1;
        private DataGridView dataGridView1;
        private RichTextBox txtCommand;
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCCommand));
            this.sysToolBar = new System.Windows.Forms.ToolStrip();
            this.tbbExcute = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtResult = new System.Windows.Forms.RichTextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtCommand = new System.Windows.Forms.RichTextBox();
            this.sysToolBar.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // sysToolBar
            // 
            this.sysToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbExcute});
            this.sysToolBar.Location = new System.Drawing.Point(0, 0);
            this.sysToolBar.Name = "sysToolBar";
            this.sysToolBar.Size = new System.Drawing.Size(717, 25);
            this.sysToolBar.TabIndex = 1;
            this.sysToolBar.Text = "toolStrip1";
            // 
            // tbbExcute
            // 
            this.tbbExcute.Image = ((System.Drawing.Image)(resources.GetObject("tbbExcute.Image")));
            this.tbbExcute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbExcute.Name = "tbbExcute";
            this.tbbExcute.Size = new System.Drawing.Size(67, 22);
            this.tbbExcute.Text = "执行(&E)";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 332);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(717, 218);
            this.tabControl1.TabIndex = 70;
            this.tabControl1.Visible = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(709, 192);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "结果";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtResult);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(709, 192);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "消息";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtResult
            // 
            this.txtResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtResult.Location = new System.Drawing.Point(0, 0);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(709, 192);
            this.txtResult.TabIndex = 72;
            this.txtResult.Text = "";
            // 
            // splitter1
            // 
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 327);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(717, 5);
            this.splitter1.TabIndex = 71;
            this.splitter1.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(709, 192);
            this.dataGridView1.TabIndex = 73;
            // 
            // txtCommand
            // 
            this.txtCommand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommand.Location = new System.Drawing.Point(0, 25);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(717, 302);
            this.txtCommand.TabIndex = 72;
            this.txtCommand.Text = "";
            // 
            // UCCommand
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtCommand);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.sysToolBar);
            this.Name = "UCCommand";
            this.Size = new System.Drawing.Size(717, 550);
            this.sysToolBar.ResumeLayout(false);
            this.sysToolBar.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TabControl tabControl = null;
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DBFileName
        {
            get;
            set;
        }

        public UCCommand()
        {
            InitializeComponent();
        }

        public UCCommand(TabControl tabControl)
        {
            this.InitializeComponent();

            this.tabControl = tabControl;
            this.Load +=UCCommand_Load;
        }

        void UCCommand_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
                return;

            this.tbbExcute.Click += tbbExcute_Click;
        }

        void tbbExcute_Click(object sender, EventArgs e)
        {
            this.Excute(this.txtCommand.Text);
        }

        private void Excute(string commandText)
        {
            if (string.IsNullOrEmpty(commandText))
                return;
            this.txtResult.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n";
            try
            {
                this.tabControl1.Visible = true;
                OleDbHelper dbHelper = new OleDbHelper(this.DBFileName);
                /*2020-07-08 侯连文 获取命令首关键，进行判断是查询或执行其他语句*/
                string command = commandText.Substring(0, commandText.IndexOf(' '));
                if (command.ToLower() == "Select".ToLower())
                {
                    DataTable dtData = dbHelper.ExecuteDataTable(commandText);

                    this.dataGridView1.AutoGenerateColumns = true;
                    this.dataGridView1.AllowUserToAddRows = false;
                    this.dataGridView1.ReadOnly = true;
                    this.dataGridView1.DataSource = dtData;

                    this.txtResult.AppendText("(" + dtData.Rows.Count + " 行受影响)");
                    if (this.tabControl1.TabPages.Contains(this.tabPage1) == false)
                        this.tabControl1.TabPages.Add(this.tabPage1);
                }
                else
                {
                    int result = dbHelper.ExecuteNonQuery(commandText);
                    this.txtResult.AppendText("(" + result + " 行受影响)");

                    if (this.tabControl1.TabPages.Contains(this.tabPage1))
                        this.tabControl1.TabPages.Remove(this.tabPage1);
                }

            }
            catch (Exception ex)
            {
                this.txtResult.AppendText("执行命令异常：" + ex.ToString() + "\r\n");
            }
        }
    }
}
