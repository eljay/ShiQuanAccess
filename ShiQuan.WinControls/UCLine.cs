﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.WinControls
{
    /// <summary>
    /// 直线的方向
    /// </summary>
    public enum DirectionMode
    {
        /// <summary>
        /// 水平
        /// </summary>
        Horizontal,

        /// <summary>
        /// 竖直
        /// </summary>
        Vertical
    }
    /// <summary>
    /// 
    /// </summary>
    public class UCLine : Control
    {
        private DirectionMode m_DirectionMode;

        /// <summary>
        /// 设置直线方向
        /// </summary>
        [Category("自定项目")]
        [Description("设置直线方向。")]
        [DefaultValueAttribute(DirectionMode.Horizontal)]
        public DirectionMode Direction
        {
            get { return m_DirectionMode; }
            set
            {
                if (m_DirectionMode != value)
                {
                    Size = new Size(Size.Height, Size.Width);
                }
                m_DirectionMode = value;
                Invalidate();
            }
        }
    }
}
