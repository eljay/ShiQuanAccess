﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ShiQuan.Access.Helper;

namespace ShiQuan.WinControls
{
    public class UCTableInfo : UserControl
    {
        private ToolStrip sysToolBar;
        private ToolStripButton tbbRefresh;
        private DataGridView dataGridView1;
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCTableInfo));
            this.sysToolBar = new System.Windows.Forms.ToolStrip();
            this.tbbRefresh = new System.Windows.Forms.ToolStripButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.sysToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // sysToolBar
            // 
            this.sysToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbRefresh});
            this.sysToolBar.Location = new System.Drawing.Point(0, 0);
            this.sysToolBar.Name = "sysToolBar";
            this.sysToolBar.Size = new System.Drawing.Size(712, 25);
            this.sysToolBar.TabIndex = 1;
            this.sysToolBar.Text = "toolStrip1";
            // 
            // tbbRefresh
            // 
            this.tbbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tbbRefresh.Image")));
            this.tbbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbRefresh.Name = "tbbRefresh";
            this.tbbRefresh.Size = new System.Drawing.Size(67, 22);
            this.tbbRefresh.Text = "刷新(&R)";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(712, 489);
            this.dataGridView1.TabIndex = 2;
            // 
            // UCTableInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.sysToolBar);
            this.Name = "UCTableInfo";
            this.Size = new System.Drawing.Size(712, 514);
            this.sysToolBar.ResumeLayout(false);
            this.sysToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TabControl tabControl;

        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DBFileName
        {
            get;
            set;
        }
        /// <summary>
        /// 数据表名称
        /// </summary>
        public string TableName
        {
            get;
            set;
        }

        public UCTableInfo()
        {
            InitializeComponent();
        }

        public UCTableInfo(TabControl tabControl)
        {
            InitializeComponent();

            this.tabControl = tabControl;
            this.Load +=UCTableInfo_Load;
        }

        void UCTableInfo_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
                return;

            this.tbbRefresh.Click += tbbRefresh_Click;
            this.LoadData();
        }

        void tbbRefresh_Click(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void LoadData()
        {
            try
            {
                this.dataGridView1.AutoGenerateColumns = true;
                this.dataGridView1.AllowUserToAddRows = false;
                this.dataGridView1.ReadOnly = true;
                OleDbHelper dbHelper = new OleDbHelper(DBFileName);
                DataTable dtData = dbHelper.GetColumns(TableName);
                DataView dvData = new DataView(dtData, "", "Position", DataViewRowState.CurrentRows);
                this.dataGridView1.DataSource = dvData;
            }
            catch (Exception ex)
            {
                WinMessageBox.ShowError("读取数据表列异常：" + ex.Message);
            }

        }
    }
}
