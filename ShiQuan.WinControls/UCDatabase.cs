﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ShiQuan.Access.Helper;

namespace ShiQuan.WinControls
{
    public class UCDatabase: UserControl
    {
        private ToolStrip sysToolBar;
        private RichTextBox richTextBox1;
        private GroupBox groupBox1;
        private ImageList imageList1;
        private Splitter splitter1;
        private TabControl tabControl1;
        private ToolStripButton tbbCommand;
        private ToolStripButton tbbOpen;
        private ToolStripButton tbbCopy;
        private ToolStripButton tbbInfo;
        private ToolStripSeparator toolStripSeparator1;
        private TreeView treeView1;
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCDatabase));
            this.sysToolBar = new System.Windows.Forms.ToolStrip();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbbCommand = new System.Windows.Forms.ToolStripButton();
            this.tbbCopy = new System.Windows.Forms.ToolStripButton();
            this.tbbOpen = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbbInfo = new System.Windows.Forms.ToolStripButton();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.sysToolBar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sysToolBar
            // 
            this.sysToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbOpen,
            this.tbbCopy,
            this.tbbInfo,
            this.toolStripSeparator1,
            this.tbbCommand});
            this.sysToolBar.Location = new System.Drawing.Point(0, 0);
            this.sysToolBar.Name = "sysToolBar";
            this.sysToolBar.Size = new System.Drawing.Size(729, 25);
            this.sysToolBar.TabIndex = 0;
            this.sysToolBar.Text = "toolStrip1";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Info;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.richTextBox1.Location = new System.Drawing.Point(0, 477);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(729, 30);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.treeView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 452);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "FileClose.gif");
            this.imageList1.Images.SetKeyName(1, "FileOpen.gif");
            // 
            // splitter1
            // 
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitter1.Location = new System.Drawing.Point(200, 25);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 452);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(205, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(524, 452);
            this.tabControl1.TabIndex = 4;
            // 
            // tbbCommand
            // 
            this.tbbCommand.Image = ((System.Drawing.Image)(resources.GetObject("tbbCommand.Image")));
            this.tbbCommand.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbCommand.Name = "tbbCommand";
            this.tbbCommand.Size = new System.Drawing.Size(67, 22);
            this.tbbCommand.Text = "SQL命令";
            // 
            // tbbCopy
            // 
            this.tbbCopy.Image = ((System.Drawing.Image)(resources.GetObject("tbbCopy.Image")));
            this.tbbCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbCopy.Name = "tbbCopy";
            this.tbbCopy.Size = new System.Drawing.Size(67, 22);
            this.tbbCopy.Text = "复制(&C)";
            // 
            // tbbOpen
            // 
            this.tbbOpen.Image = ((System.Drawing.Image)(resources.GetObject("tbbOpen.Image")));
            this.tbbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbOpen.Name = "tbbOpen";
            this.tbbOpen.Size = new System.Drawing.Size(67, 22);
            this.tbbOpen.Text = "打开(&O)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbbInfo
            // 
            this.tbbInfo.Image = ((System.Drawing.Image)(resources.GetObject("tbbInfo.Image")));
            this.tbbInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbInfo.Name = "tbbInfo";
            this.tbbInfo.Size = new System.Drawing.Size(67, 22);
            this.tbbInfo.Text = "属性(&I)";
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(3, 17);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 1;
            this.treeView1.Size = new System.Drawing.Size(194, 432);
            this.treeView1.TabIndex = 0;
            // 
            // UCDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.sysToolBar);
            this.Name = "UCDatabase";
            this.Size = new System.Drawing.Size(729, 507);
            this.sysToolBar.ResumeLayout(false);
            this.sysToolBar.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 数据库文件
        /// </summary>
        public string DBFileName
        {
            get { return this.richTextBox1.Text; }
            set { this.richTextBox1.Text = value; }
        }
        private DataTable dtTables = null;
        private string DefaultTable = "";
        public UCDatabase()
        {
            InitializeComponent();

            this.Load += UCDatabase_Load;
        }

        void UCDatabase_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
                return;

            this.tbbCommand.Click += tbbCommand_Click;
            this.tbbCopy.Click +=tbbCopy_Click;
            this.tbbOpen.Click +=tbbOpen_Click;
            this.tbbInfo.Click +=tbbInfo_Click;
        }

        /// <summary>
        /// 加载数据
        /// </summary>
        public void LoadData(string dbFileName)
        {
            this.DBFileName = dbFileName;

            this.LoadTables();
            /*加载默认数据表或第一个数据表*/
            if (string.IsNullOrEmpty(this.DefaultTable) && dtTables.Rows.Count > 0)
                this.DefaultTable = dtTables.Rows[0]["Table_Name"].ToString();
            if (string.IsNullOrEmpty(this.DefaultTable) == false)
                this.OpenTable(this.DefaultTable);//默认加载TestData
        }

        void tbbCopy_Click(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode == null || this.treeView1.SelectedNode.Parent == null)
            {
                WinMessageBox.ShowWarning("请选择数据表！");
                return;
            }

            FrmCopy frmCopy = new FrmCopy();
            frmCopy.DBFileName = this.DBFileName;
            frmCopy.TableName = this.treeView1.SelectedNode.Name;
            if (frmCopy.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            this.LoadTables();
        }

        private void LoadTables()
        {
            OleDbHelper dbHelper = new OleDbHelper(this.DBFileName);
            dtTables = dbHelper.GetTables();

            this.treeView1.DoubleClick -= treeView1_DoubleClick;
            this.treeView1.Nodes.Clear();
            TreeNode root = new TreeNode();
            root.Name = "所有数据表";
            root.Text = "所有数据表";

            TreeNode node = null;
            foreach (DataRow drData in dtTables.Rows)
            {
                node = new TreeNode();
                node.Name = drData["Table_Name"].ToString();
                node.Text = drData["Table_Name"].ToString();
                root.Nodes.Add(node);

                if (node.Name.ToLower() == "TestData".ToLower())
                    this.DefaultTable = "TestData";
            }
            root.Expand();
            this.treeView1.Nodes.Add(root);
            this.treeView1.DoubleClick += treeView1_DoubleClick;
            this.treeView1.SelectedNode = root;
        }

        void tbbInfo_Click(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode == null || this.treeView1.SelectedNode.Parent == null)
            {
                WinMessageBox.ShowWarning("请选择数据表！");
                return;
            }
            var ucName = "ucTableInfo";
            if (this.tabControl1.TabPages.ContainsKey(ucName) == false)
            {
                UCTableInfo ucTableInfo = new UCTableInfo(this.tabControl1);
                ucTableInfo.Dock = System.Windows.Forms.DockStyle.Fill;
                ucTableInfo.DBFileName = DBFileName;
                ucTableInfo.TableName = this.treeView1.SelectedNode.Name;
                ucTableInfo.Text = ucTableInfo.Name = this.treeView1.SelectedNode.Name + "属性";

                TabPage tabPage = new TabPage();
                tabPage.Name = ucName;
                tabPage.Text = ucTableInfo.Text;
                tabPage.Controls.Add(ucTableInfo);

                this.tabControl1.TabPages.Add(tabPage);
            }
            this.tabControl1.SelectedTab = this.tabControl1.TabPages[ucName];
        }

        void tbbCommand_Click(object sender, EventArgs e)
        {
            var ucName = "ucCommand";
            if (this.tabControl1.TabPages.ContainsKey(ucName) == false)
            {
                UCCommand ucCommand = new UCCommand(this.tabControl1);
                ucCommand.Dock = System.Windows.Forms.DockStyle.Fill;
                ucCommand.DBFileName = DBFileName;
                ucCommand.Text = ucCommand.Name = "SQL命令";

                TabPage tabPage = new TabPage();
                tabPage.Name = ucName;
                tabPage.Text = ucCommand.Text;
                tabPage.Controls.Add(ucCommand);

                this.tabControl1.TabPages.Add(tabPage);
            }
            this.tabControl1.SelectedTab = this.tabControl1.TabPages[ucName];
        }

        void treeView1_DoubleClick(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode == null || this.treeView1.SelectedNode.Parent == null)
            {
                WinMessageBox.ShowWarning("请选择数据表！");
                return;
            }
            this.OpenTable(this.treeView1.SelectedNode.Name);
        }


        void tbbOpen_Click(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode == null || this.treeView1.SelectedNode.Parent == null)
            {
                WinMessageBox.ShowWarning("请选择数据表！");
                return;
            }
            this.OpenTable(this.treeView1.SelectedNode.Name);
        }

        private void OpenTable(string tableName)
        {
            if (this.tabControl1.TabPages.ContainsKey(tableName) == false)
            {
                UCDataTable ucDataTable = new UCDataTable(this.tabControl1);
                ucDataTable.Dock = System.Windows.Forms.DockStyle.Fill;
                ucDataTable.DBFileName = DBFileName;
                ucDataTable.TableName = tableName;
                ucDataTable.Text = ucDataTable.Name = tableName;

                TabPage tabPage = new TabPage();
                tabPage.Name = tabPage.Text = tableName;
                tabPage.Controls.Add(ucDataTable);

                this.tabControl1.TabPages.Add(tabPage);
            }
            this.tabControl1.SelectedTab = this.tabControl1.TabPages[tableName];
        }
    }
}
