﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ShiQuan.Access.Helper
{
    /// <summary>
    /// 日志
    /// </summary>
    public class Logger
    {
        static object lockobj = new object();
        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="title"></param>
        /// <param name="msg"></param>
        public static void Info(string title, string msg)
        {
            //根据日期创建文件(格式Log_年年年年月月日日)
            lock (lockobj)
            {
                StringBuilder fileContent = new StringBuilder();
                fileContent.AppendLine("====");
                fileContent.AppendLine("now=" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                fileContent.AppendLine("title=" + title);
                fileContent.AppendLine("msg=" + msg);

                try
                {
                    var savePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
                    //如果没有创建文件
                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }
                    string filePath = savePath + @"\" + DateTime.Now.ToString("yyyyMMddHH") + ".log";
                    using (StreamWriter sw = new StreamWriter(filePath, true))
                    {
                        sw.Write(fileContent.ToString());
                    }
                }
                catch (Exception)
                {
                    //权限不足的可能
                    string filePath = System.Environment.CurrentDirectory + @"\log" + @"\" + DateTime.Now.ToString("yyyyMMdd") + ".log";
                    if (!Directory.Exists(System.Environment.CurrentDirectory + @"\log"))
                    {
                        Directory.CreateDirectory(System.Environment.CurrentDirectory + @"\log");
                    }
                    using (StreamWriter sw = new StreamWriter(filePath, true))
                    {
                        sw.Write(fileContent.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="title"></param>
        /// <param name="msg"></param>
        public static void Error(string title, Exception msg)
        {
            //根据日期创建文件(格式Log_年年年年月月日日)
            lock (lockobj)
            {
                StringBuilder fileContent = new StringBuilder();
                fileContent.AppendLine("====");
                fileContent.AppendLine("now=" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                fileContent.AppendLine("title=" + title);
                if (msg != null)
                    fileContent.AppendLine("msg=" + msg.ToString());

                try
                {
                    var savePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
                    //如果没有创建文件
                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }
                    string filePath = savePath + @"\" + DateTime.Now.ToString("yyyyMMddHH") + ".log";
                    using (StreamWriter sw = new StreamWriter(filePath, true))
                    {
                        sw.Write(fileContent.ToString());
                    }
                }
                catch (Exception)
                {
                    //权限不足的可能
                    string filePath = System.Environment.CurrentDirectory + @"\log" + @"\" + DateTime.Now.ToString("yyyyMMdd") + ".log";
                    if (!Directory.Exists(System.Environment.CurrentDirectory + @"\log"))
                    {
                        Directory.CreateDirectory(System.Environment.CurrentDirectory + @"\log");
                    }
                    using (StreamWriter sw = new StreamWriter(filePath, true))
                    {
                        sw.Write(fileContent.ToString());
                    }
                }
            }
        }
    }
}
