﻿using ShiQuan.WinControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Access
{
    public  class FrmMain : Form
    {
        private ToolStrip toolStrip1;
        private ToolStripButton tbbOpen;
        private ToolStripDropDownButton tbbHelper;
        private ToolStripMenuItem mnuAbout;
        private StatusStrip sysStatusBar;
        private ToolStripStatusLabel sysStatusLabel;
        private DataGridView dataGridView1;
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.sysStatusBar = new System.Windows.Forms.StatusStrip();
            this.sysStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tbbOpen = new System.Windows.Forms.ToolStripButton();
            this.tbbHelper = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolStrip1.SuspendLayout();
            this.sysStatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbOpen,
            this.tbbHelper});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(592, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // sysStatusBar
            // 
            this.sysStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sysStatusLabel});
            this.sysStatusBar.Location = new System.Drawing.Point(0, 451);
            this.sysStatusBar.Name = "sysStatusBar";
            this.sysStatusBar.Size = new System.Drawing.Size(592, 22);
            this.sysStatusBar.SizingGrip = false;
            this.sysStatusBar.TabIndex = 1;
            // 
            // sysStatusLabel
            // 
            this.sysStatusLabel.Name = "sysStatusLabel";
            this.sysStatusLabel.Size = new System.Drawing.Size(577, 17);
            this.sysStatusLabel.Spring = true;
            this.sysStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbbOpen
            // 
            this.tbbOpen.Image = ((System.Drawing.Image)(resources.GetObject("tbbOpen.Image")));
            this.tbbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbOpen.Name = "tbbOpen";
            this.tbbOpen.Size = new System.Drawing.Size(67, 22);
            this.tbbOpen.Text = "打开(&O)";
            // 
            // tbbHelper
            // 
            this.tbbHelper.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbbHelper.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAbout});
            this.tbbHelper.Image = ((System.Drawing.Image)(resources.GetObject("tbbHelper.Image")));
            this.tbbHelper.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbHelper.Name = "tbbHelper";
            this.tbbHelper.Size = new System.Drawing.Size(58, 22);
            this.tbbHelper.Text = "帮助";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(152, 22);
            this.mnuAbout.Text = "关于";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(592, 426);
            this.dataGridView1.TabIndex = 2;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 473);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.sysStatusBar);
            this.Controls.Add(this.toolStrip1);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.sysStatusBar.ResumeLayout(false);
            this.sysStatusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public FrmMain()
        {
            InitializeComponent();

            this.Load += FrmMain_Load;
        }
        private string FileName = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"access.xml");
        private DataTable dtData = new DataTable("access");
        void FrmMain_Load(object sender, EventArgs e)
        {
            this.Text = Application.ProductName + "("+Application.ProductVersion+")";
            this.sysStatusLabel.Text = "技术支持：896374871@qq.com";

            this.mnuAbout.Click += mnuAbout_Click;
            this.tbbOpen.Click += tbbOpen_Click;

            /*2020-09-01 侯连文 初始界面*/
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            DataGridViewTextBoxColumn dc = new DataGridViewTextBoxColumn();
            dc.HeaderText = "文件名称";
            dc.Name = dc.DataPropertyName = "FileName";
            dc.Width = 100;
            this.dataGridView1.Columns.Add(dc);

            dc = new DataGridViewTextBoxColumn();
            dc.HeaderText = "文件地址";
            dc.Name = dc.DataPropertyName = "FilePath";
            dc.Width = 300;
            this.dataGridView1.Columns.Add(dc);

            this.LoadData();
            this.dataGridView1.CellContentDoubleClick += dataGridView1_CellContentDoubleClick;
        }

        void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dataGridView1.CurrentCell == null)
                return;
            int rowIndex = this.dataGridView1.CurrentCell.RowIndex;
            DataRowView drv = this.dataGridView1.Rows[rowIndex].DataBoundItem as DataRowView;
            if(System.IO.File.Exists(drv["FilePath"].ToString()) == false)
                return;

            this.OpenAccess(drv["FilePath"].ToString());
        }

        private void LoadData()
        {
            if(System.IO.File.Exists(FileName))
            {
                dtData.ReadXml(FileName);
            }
            else
            {
                dtData.Columns.Add("FileName");
                dtData.Columns.Add("FilePath");
            }
            this.dataGridView1.DataSource = dtData;
        }

        void mnuAbout_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        void tbbOpen_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Access 数据库|*.mdb";
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            this.OpenAccess(dialog.FileName);
        }

        private void OpenAccess(string filePath)
        {
            FrmAccess frm = new FrmAccess();
            frm.DBFileName = filePath;
            frm.Show();

            try
            {
                DataRow[] drData = dtData.Select("FilePath='" + filePath + "'");
                if (drData.Length > 0)
                    return;

                FileInfo fileInfo = new FileInfo(filePath);
                DataRow row = dtData.NewRow();
                row["FileName"] = fileInfo.Name;
                row["FilePath"] = fileInfo.FullName;
                dtData.Rows.Add(row);

                dtData.WriteXml(FileName,XmlWriteMode.WriteSchema);

                this.LoadData();
            }
            catch (Exception ex)
            {
                WinMessageBox.ShowError("保存数据异常：" + ex.Message);
            }
        }
    }
}
