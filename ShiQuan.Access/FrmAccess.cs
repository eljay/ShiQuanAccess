﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Access
{
    public class FrmAccess : Form
    {
        private WinControls.UCDatabase ucDatabase1;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucDatabase1 = new ShiQuan.WinControls.UCDatabase();
            this.SuspendLayout();
            // 
            // ucDatabase1
            // 
            this.ucDatabase1.DBFileName = "";
            this.ucDatabase1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucDatabase1.Location = new System.Drawing.Point(0, 0);
            this.ucDatabase1.Name = "ucDatabase1";
            this.ucDatabase1.Size = new System.Drawing.Size(770, 598);
            this.ucDatabase1.TabIndex = 0;
            // 
            // FrmAccess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 598);
            this.Controls.Add(this.ucDatabase1);
            this.Name = "FrmAccess";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "数据库管理";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        /// <summary>
        /// 数据库文件
        /// </summary>
        public string DBFileName
        {
            get;
            set;
        }

        public FrmAccess()
        {
            InitializeComponent();

            this.Load += FrmAccess_Load;
        }

        void FrmAccess_Load(object sender, EventArgs e)
        {
            this.ucDatabase1.LoadData(this.DBFileName);
        }
    }
}
