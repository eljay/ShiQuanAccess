# ShiQuan.Access

#### 介绍
Access 数据库管理

#### 软件架构
软件架构说明
基于VS2015 C# OleDb 开发，进行Access 数据操作管理。

#### 安装教程

绿色版本，解压运行

#### 使用说明

1.  打开数据表编辑
![输入图片说明](https://images.gitee.com/uploads/images/2020/0902/103540_71160789_1063145.png "2020-09-02_102854.png")

2.  复制数据表
![输入图片说明](https://images.gitee.com/uploads/images/2020/0902/103605_58dad870_1063145.png "复制.png")

3.  SQL 命令执行
![输入图片说明](https://images.gitee.com/uploads/images/2020/0902/103619_86240362_1063145.png "SqL 语句.png")

更多功能，敬请期待！

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
